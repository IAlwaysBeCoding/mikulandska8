# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy import Field, Item

class JobItem(Item):

    series = Field()
    job_url = Field()
    series_page_url = Field()
    job_title = Field()
    department = Field()
    agency = Field()
    opening_date = Field()
    closing_date = Field()
    payscale_grade = Field()
    minimum_salary = Field()
    maximum_salary = Field()
    salary_term = Field()
    work_schedule = Field()
    appointment_type = Field()
    job_open_to = Field()
    location = Field()
    announcement_number = Field()
    control_number = Field()
    has_apply_button = Field()
    series_name_on_job = Field()
    series_link_on_job = Field()
    main_content = Field()




