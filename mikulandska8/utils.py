
def strip_spaces(raw_str):
    import sys
    if sys.version_info[0] < 3:
        if isinstance(raw_str, (unicode, str)):
            clean = raw_str.strip()

            return clean.replace('\n', ' ').replace('\t','')
        else:
            return raw_str
    else:
        if isinstance(raw_str, str):
            clean = raw_str.strip()

            return clean.replace('\n', ' ').replace('\t','')
        else:
            return raw_str

