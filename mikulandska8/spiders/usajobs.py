# -*- coding: utf-8 -*
import json
from datetime import datetime

from furl import furl
import arrow
from scrapy import Spider, Request, Selector
from scrapy.loader.processors import SelectJmes
from mikulandska8.items import JobItem
from mikulandska8.series import series as series_code
from mikulandska8.utils import strip_spaces


class UsajobsSpider(Spider):
    name = 'usajobs'
    allowed_domains = ['usajobs.gov']
    start_urls = ['http://usajobs.gov/']

    def __init__(self, *args, **kwargs):
        page = kwargs.get('page', 1)

        self.opening_date = kwargs.get('opening_date', None)
        self.closing_date = kwargs.get('closing_date', self.opening_date)

        series = kwargs.get('series', None)
        self.series = self._parse_series_range(series)

        self.limit_date_range = True if self.opening_date else False
        if self.limit_date_range:
            self.date_range = self._parse_date_range(self.opening_date, self.closing_date)
        else:
            self.date_range = []
        self.page = int(page)

    def _parse_date_range(self, start_date, end_date):
        start_date = arrow.get(start_date, 'MM/DD/YYYY')
        end_date = arrow.get(end_date, 'MM/DD/YYYY')
        return [d.format('MM/DD/YYYY') for d in arrow.Arrow.range('day', start_date, end_date)]

    def _parse_series_range(self, series):

        if not series:
            return [s for s in series_code]
        else:
            return [s for s in series.split(',')]

    def parse(self, response):
        pass
    def start_requests(self):

        for serie in self.series:
            f = furl('https://www.usajobs.gov/Search/ExploreOpportunities/')
            f.args['Series'] = serie
            f.args['page']  = self.page
            f.args['sort'] = 'enddate'
            url = f.url
            yield Request(url,
                        meta={
                            'series': serie,
                            'page' : self.page,
                            'limit_date_range' : self.limit_date_range
                        },
                        callback=self.job_results)

    def job_results(self, response):

        query = {
            "JobTitle": [],
            "GradeBucket": [],
            "JobCategoryCode": [
                response.meta['series']
            ],
            "JobCategoryFamily": [],
            "LocationName": [],
            "Page": str(response.meta['page']),
            "PostingChannel": [],
            "Department": [],
            "Agency": [],
            "PositionOfferingTypeCode": [],
            "TravelPercentage": [],
            "PositionScheduleTypeCode": [],
            "SecurityClearanceRequired": [],
            "ShowAllFilters": [],
            "SortField" : "startdate",
            "HiringPath": [],
            "SocTitle": [],
            "UniqueSearchID": response.xpath('//input[@id="UniqueSearchID"]/@value').extract_first(),
            "IsAuthenticated": False,
        }


        return  Request('https://www.usajobs.gov/Search/ExecuteSearch',
                    method='POST',
                    headers={
                        'Content-Type' : 'application/json; charset=utf-8',
                    },
                    body = json.dumps(query),
                    meta=response.meta,
                    callback=self.parse_job_results)

    def parse_job_results(self, response):

        current_page = response.meta['page']
        serie = response.meta['series']
        limit_date_range = response.meta['limit_date_range']

        jobs =  SelectJmes('Jobs[].{job_id : DocumentID, date : DateDisplay}')(json.loads(response.body))
        number_of_pages = SelectJmes('Pager.NumberOfPages')(json.loads(response.body))

        found_jobs = False
        for job in jobs:
            document_id = job['job_id']

            opening_date = job['date'].split(' to ')[0].replace('Open ', '')
            closing_date = job['date'].split(' to ')[1]

            url = response.urljoin('/GetJob/ViewDetails/{}'.format(document_id))

            date_range = self._parse_date_range(opening_date, closing_date)
            response.meta['series_page_url'] = 'https://www.usajobs.gov/Search?j={}&p={}&s=startdate'.format(serie, current_page)
            #self.log(url)

            if ((opening_date in self.date_range) and (closing_date in self.date_range)) or (not limit_date_range):
                found_jobs = True
                yield Request(url,
                              meta=response.meta,
                              callback=self.parse_job_item)

        if number_of_pages and number_of_pages != int(current_page):
            if (limit_date_range and found_jobs) or (not limit_date_range):
                next_page = self.paginate(response)
                if next_page:
                    yield next_page
            else:
                self.log(not limit_date_range)
                self.log(limit_date_range and found_jobs)


    def paginate(self, response):

        current_page = response.meta['page']
        has_next_page = response.xpath('//li[@class="usajobs-search-pagination__next-page-container is-disabled"]').extract_first()

        if not has_next_page:
            f = furl(response.url)
            f.args['page'] = current_page + 1
            url = f.url
            response.meta['page'] = current_page + 1
            return  Request(url,
                            meta=response.meta,
                            callback=self.job_results)

    def parse_job_item(self, response):

        job_item = JobItem()
        overview = self.parse_overview(response)
        job_headers = self.parse_job_headers(response)
        job_open_to = self.parse_job_open_to(response)
        location = self.parse_location(response)
        announcement_control_number = self.parse_announcement_control_number(response)
        main_content = self.parse_main_content(response)
        series = response.meta['series']

        job_item['series'] = response.meta['series']
        job_item['job_url'] = response.url
        job_item['series_page_url'] = response.meta['series_page_url']
        job_item['job_title'] = job_headers['job_title']
        job_item['department'] = job_headers['department']
        job_item['agency'] = job_headers['agency']
        job_item['opening_date'] = overview['opening_date']
        job_item['closing_date'] = overview['closing_date']
        job_item['payscale_grade'] = overview['payscale_grade']
        job_item['minimum_salary'] = overview['minimum_salary']
        job_item['maximum_salary'] = overview['maximum_salary']
        job_item['salary_term'] = overview['salary_term']
        job_item['work_schedule'] = overview['work_schedule']
        job_item['appointment_type'] = overview['appointment_type']
        job_item['location'] = location['location']
        job_item['job_open_to'] = job_open_to
        job_item['announcement_number'] = announcement_control_number['announcement_number']
        job_item['control_number'] = announcement_control_number['control_number']
        job_item['has_apply_button'] = announcement_control_number['has_apply_button']
        #job_item['series_name_on_job'] = series['series_name_on_job']
        #job_item['series_link_on_job'] = series['series_link_on_job']
        job_item['main_content'] = main_content


        return job_item

    def parse_opening_closing_date(self, response):
        pass
    def parse_overview(self, response):

        overview = {}
        ovitems = response.xpath('//ul[@class="usajobs-joa-summary__list"]/li').extract()

        for ovitem in ovitems:
            ovsel = Selector(text=ovitem)
            label = ovsel.xpath('//h5[@class="usajobs-joa-summary__label"]/span/text()').extract_first()

            if label:
                clean_label = strip_spaces(label)
                if clean_label == 'Open & closing dates':
                    overview['opening_date'] = ovsel.xpath('//*[@class="usajobs-joa-summary__value"]//span[1]/text()').extract_first()
                    overview['closing_date'] = ovsel.xpath('//*[@class="usajobs-joa-summary__value"]//span[2]/text()').extract_first()

                elif clean_label == 'Pay scale & grade':
                    payscale_grade = ovsel.xpath('//*[@class="usajobs-joa-summary__grades"]/text()').extract_first()
                    overview['payscale_grade'] =  strip_spaces(payscale_grade) if payscale_grade else ''


                elif clean_label == 'Salary':
                    raw_salary = ovsel.xpath('//*[@class="usajobs-joa-summary__salary"]//span[1]/text()').extract_first()
                    split_salary = raw_salary.split(' to ')
                    if len(split_salary) == 2:
                        overview['minimum_salary'] = split_salary[0]

                        per_split = split_salary[1].split(' per ')

                        if len(per_split) == 2:
                            overview['maximum_salary'] = per_split[0]
                            overview['salary_term'] = per_split[1]
                        else:
                            overview['maximum_salary'] = split_salary[1]
                            overview['salary_term'] = ''
                    else:

                            overview['minimum_salary'] = ''
                            overview['minimum_salary'] = ''


                elif clean_label == 'Work schedule':
                    work_schedule = ovsel.xpath('//*[@class="usajobs-joa-summary__value"]/text()').extract_first()
                    overview['work_schedule'] = strip_spaces(work_schedule) if work_schedule else ''


                elif clean_label == 'Appointment type':
                    appointment_type = ovsel.xpath('//*[@class="usajobs-joa-summary__value"]/text()').extract_first()
                    overview['appointment_type'] = strip_spaces(appointment_type) if appointment_type else ''


        return overview

    def parse_job_headers(self, response):

        headers = {}
        job_title = response.xpath('//*[@id="job-title"]/text()').extract_first()
        department = response.xpath('//h5[@class="usajobs-joa-banner__dept"]/text()').extract_first()
        agency = response.xpath('//a[@href="#agency-modal-trigger"]/text()').extract_first()

        headers['job_title'] = strip_spaces(job_title) if job_title else ''
        headers['department'] = strip_spaces(department) if department else ''
        headers['agency'] = strip_spaces(agency) if agency else ''

        return headers

    def parse_job_open_to(self, response):

        job_open_to = {}

        hiring_items = response.xpath('//ul[@class="usajobs-joa-intro-hiring-paths__list"]/li').extract()

        for hiring_item in hiring_items:

            hsel = Selector(text=hiring_item)
            title = hsel.xpath('//h4[@class="usajobs-joa-intro-hiring-paths__title"]/text()').extract_first()
            title = strip_spaces(title) if title else ''

            if title != '':
                detail = hsel.xpath('//p[@class="usajobs-joa-intro-hiring-paths__detail"]/text()').extract()
                detail = "".join([strip_spaces(d) for d in detail]) if detail else ''

                sub_detail = hsel.xpath('//li[@class="usajobs-joa-intro-hiring-paths__sublist-item"]/text()').extract()
                if sub_detail:
                    all_sub_details = "*".join(sub_detail)
                    job_open_to[title] = detail +'|||'+ all_sub_details

                else:
                    job_open_to[title] = detail


        return job_open_to

    def parse_location(self, response):

        location = {}

        location_city = response.xpath('//span[@class="usajobs-joa-locations__city"]/text()').extract_first()
        location_street = response.xpath('//span[@class="usajobs-joa-locations__street"]/text()').extract_first()
        location_city = strip_spaces(location_city) if location_city else ''
        location_street = strip_spaces(location_street) if location_street else ''

        location_detail = ",".join([s for s in (location_street, location_city) if s != ''])
        location['location'] = location_detail

        return location

    def parse_announcement_control_number(self, response):

       numbers = {}

       announcement_number = response.xpath('//input[@id="AnnouncementID"]/@value').extract_first()
       control_number = response.xpath('//input[@id="ControlNumber"]/@value').extract_first()
       has_apply_button = response.xpath('//div[@class="usajobs-joa-actions__col-apply usajobs-joa-actions--v1-5__col-apply"]').extract_first()

       numbers['announcement_number'] = announcement_number if announcement_number else ''
       numbers['control_number'] = control_number if control_number else ''
       numbers['has_apply_button'] = 'Apply' if has_apply_button else ''

       return numbers

    def parse_job_series(self, response):

        series = {}

        series_name_on_job = response.xpath('//p[@class="usajobs-joa-summary__series"]/a/text()').extract_first()
        series_link_on_job = response.xpath('//p[@class="usajobs-joa-summary__series"]/a/@href').extract_first()

        series['series_name_on_job'] = strip_spaces(series_name_on_job) if series_name_on_job else ''
        series['series_link_on_job'] = response.urljoin(series_link_on_job) if series_link_on_job else ''

        return series

    def parse_main_content(self, response):

        def parse_li(li):

            lisel = Selector(text=li)
            header = lisel.xpath('//*[@class="usajobs-joa-section__header"]/text()').extract_first()
            litext = None

            if header:
                litext = lisel.xpath('//text()').extract()
                return (header, litext)

            return (header, litext)


        main_content = {}
        for li in response.xpath('//ul[@class="usa-unstyled-list"]/li').extract():
            header, litext = parse_li(li)

            if header and litext:
                main_content[header] = litext

        return main_content

